$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("ApiTestingWeather.feature");
formatter.feature({
  "line": 2,
  "name": "As a user I want to verify open weather API by City Name",
  "description": "",
  "id": "as-a-user-i-want-to-verify-open-weather-api-by-city-name",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@OpenWeatherAPI"
    }
  ]
});
formatter.scenarioOutline({
  "line": 4,
  "name": "Retrieve current open weather map using API",
  "description": "",
  "id": "as-a-user-i-want-to-verify-open-weather-api-by-city-name;retrieve-current-open-weather-map-using-api",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 5,
  "name": "I navigate to  Weather API",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I verify weather by \"\u003cCityName\u003e\" for open weather map API",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I validate the response which returned from API  \u003cResponse\u003e",
  "keyword": "Then "
});
formatter.examples({
  "line": 8,
  "name": "",
  "description": "",
  "id": "as-a-user-i-want-to-verify-open-weather-api-by-city-name;retrieve-current-open-weather-map-using-api;",
  "rows": [
    {
      "cells": [
        "CityName",
        "Response"
      ],
      "line": 9,
      "id": "as-a-user-i-want-to-verify-open-weather-api-by-city-name;retrieve-current-open-weather-map-using-api;;1"
    },
    {
      "cells": [
        "uk",
        "200"
      ],
      "line": 10,
      "id": "as-a-user-i-want-to-verify-open-weather-api-by-city-name;retrieve-current-open-weather-map-using-api;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 10,
  "name": "Retrieve current open weather map using API",
  "description": "",
  "id": "as-a-user-i-want-to-verify-open-weather-api-by-city-name;retrieve-current-open-weather-map-using-api;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@OpenWeatherAPI"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "I navigate to  Weather API",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I verify weather by \"uk\" for open weather map API",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I validate the response which returned from API  200",
  "matchedColumns": [
    1
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefinationsWeather.i_navigate_to_Weather_API()"
});
formatter.result({
  "duration": 176680178,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "uk",
      "offset": 21
    }
  ],
  "location": "StepDefinationsWeather.i_verify_weather_by_for_open_weather_map_API(String)"
});
formatter.result({
  "duration": 3388750047,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 49
    }
  ],
  "location": "StepDefinationsWeather.i_validate_the_response_which_returned_from_API(int)"
});
formatter.result({
  "duration": 4497129,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 12,
  "name": "Validate Response Message for API",
  "description": "",
  "id": "as-a-user-i-want-to-verify-open-weather-api-by-city-name;validate-response-message-for-api",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 13,
  "name": "I navigate to  Weather API",
  "keyword": "Given "
});
formatter.step({
  "line": 14,
  "name": "I verify weather by \"\u003cCityName\u003e\" for open weather map API",
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "Validate the response from weathermap API is 200",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "I verify the weather data main details \"\u003cfieldname\u003e\" and \"\u003cvalue\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "line": 18,
  "name": "",
  "description": "",
  "id": "as-a-user-i-want-to-verify-open-weather-api-by-city-name;validate-response-message-for-api;",
  "rows": [
    {
      "cells": [
        "CityName",
        "fieldname",
        "value"
      ],
      "line": 19,
      "id": "as-a-user-i-want-to-verify-open-weather-api-by-city-name;validate-response-message-for-api;;1"
    },
    {
      "cells": [
        "London",
        "main.pressure",
        "1012"
      ],
      "line": 20,
      "id": "as-a-user-i-want-to-verify-open-weather-api-by-city-name;validate-response-message-for-api;;2"
    },
    {
      "cells": [
        "London",
        "main.humidity",
        "81"
      ],
      "line": 21,
      "id": "as-a-user-i-want-to-verify-open-weather-api-by-city-name;validate-response-message-for-api;;3"
    },
    {
      "cells": [
        "London",
        "sys.country",
        "GB"
      ],
      "line": 22,
      "id": "as-a-user-i-want-to-verify-open-weather-api-by-city-name;validate-response-message-for-api;;4"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 20,
  "name": "Validate Response Message for API",
  "description": "",
  "id": "as-a-user-i-want-to-verify-open-weather-api-by-city-name;validate-response-message-for-api;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@OpenWeatherAPI"
    }
  ]
});
formatter.step({
  "line": 13,
  "name": "I navigate to  Weather API",
  "keyword": "Given "
});
formatter.step({
  "line": 14,
  "name": "I verify weather by \"London\" for open weather map API",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "Validate the response from weathermap API is 200",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "I verify the weather data main details \"main.pressure\" and \"1012\"",
  "matchedColumns": [
    1,
    2
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefinationsWeather.i_navigate_to_Weather_API()"
});
formatter.result({
  "duration": 343170,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "London",
      "offset": 21
    }
  ],
  "location": "StepDefinationsWeather.i_verify_weather_by_for_open_weather_map_API(String)"
});
formatter.result({
  "duration": 950716247,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 45
    }
  ],
  "location": "StepDefinationsWeather.validate_the_response_from_weathermap_API_is(int)"
});
formatter.result({
  "duration": 195951,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "main.pressure",
      "offset": 40
    },
    {
      "val": "1012",
      "offset": 60
    }
  ],
  "location": "StepDefinationsWeather.i_verify_the_weather_data_main_details_and(String,String)"
});
formatter.result({
  "duration": 786163493,
  "status": "passed"
});
formatter.scenario({
  "line": 21,
  "name": "Validate Response Message for API",
  "description": "",
  "id": "as-a-user-i-want-to-verify-open-weather-api-by-city-name;validate-response-message-for-api;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@OpenWeatherAPI"
    }
  ]
});
formatter.step({
  "line": 13,
  "name": "I navigate to  Weather API",
  "keyword": "Given "
});
formatter.step({
  "line": 14,
  "name": "I verify weather by \"London\" for open weather map API",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "Validate the response from weathermap API is 200",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "I verify the weather data main details \"main.humidity\" and \"81\"",
  "matchedColumns": [
    1,
    2
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefinationsWeather.i_navigate_to_Weather_API()"
});
formatter.result({
  "duration": 341119,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "London",
      "offset": 21
    }
  ],
  "location": "StepDefinationsWeather.i_verify_weather_by_for_open_weather_map_API(String)"
});
formatter.result({
  "duration": 1532029284,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 45
    }
  ],
  "location": "StepDefinationsWeather.validate_the_response_from_weathermap_API_is(int)"
});
formatter.result({
  "duration": 149272,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "main.humidity",
      "offset": 40
    },
    {
      "val": "81",
      "offset": 60
    }
  ],
  "location": "StepDefinationsWeather.i_verify_the_weather_data_main_details_and(String,String)"
});
formatter.result({
  "duration": 27615768,
  "status": "passed"
});
formatter.scenario({
  "line": 22,
  "name": "Validate Response Message for API",
  "description": "",
  "id": "as-a-user-i-want-to-verify-open-weather-api-by-city-name;validate-response-message-for-api;;4",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@OpenWeatherAPI"
    }
  ]
});
formatter.step({
  "line": 13,
  "name": "I navigate to  Weather API",
  "keyword": "Given "
});
formatter.step({
  "line": 14,
  "name": "I verify weather by \"London\" for open weather map API",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "Validate the response from weathermap API is 200",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "I verify the weather data main details \"sys.country\" and \"GB\"",
  "matchedColumns": [
    1,
    2
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefinationsWeather.i_navigate_to_Weather_API()"
});
formatter.result({
  "duration": 542200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "London",
      "offset": 21
    }
  ],
  "location": "StepDefinationsWeather.i_verify_weather_by_for_open_weather_map_API(String)"
});
formatter.result({
  "duration": 1123049594,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 45
    }
  ],
  "location": "StepDefinationsWeather.validate_the_response_from_weathermap_API_is(int)"
});
formatter.result({
  "duration": 146194,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "sys.country",
      "offset": 40
    },
    {
      "val": "GB",
      "offset": 58
    }
  ],
  "location": "StepDefinationsWeather.i_verify_the_weather_data_main_details_and(String,String)"
});
formatter.result({
  "duration": 43072308,
  "status": "passed"
});
});